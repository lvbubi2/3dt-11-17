﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]

public class PlayerController : MonoBehaviour {

    private Rigidbody RigidBody;
	// Use this for initialization
	void Start () {
        RigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 force = new Vector3(horizontal, 0.0f,vertical);

        RigidBody.AddForce(force.normalized);
	}
}
